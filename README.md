# MMIC Gain Block Template

## Features

- Board design for general MMIC amplifiers.
- SMA or MMCX connectors
- Some customizability on input/output networks.
- Allow common MMIC package footprints: SOT-89, DFN-6, SOT-86

## Documentation

Full documentation for the gain block template is available at
[RF Blocks](https://rfblocks.org/boards/GainBlock-Template.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
